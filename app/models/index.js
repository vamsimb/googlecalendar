const dbConfig = require("../config/db.config.js")

const Sequelize = require("sequelize")
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.events = require("./events.model.js")(sequelize, Sequelize)
db.junction_table = require("./junction_table.model.js")(sequelize, Sequelize)
db.guests = require("./guests.model.js")(sequelize, Sequelize)

db.events.belongsToMany(db.guests, {
  through: db.junction_table
})
db.guests.belongsToMany(db.events, {
  through: db.junction_table
})

module.exports = db