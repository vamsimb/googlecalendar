module.exports = (sequelize, Sequelize) => {
   const guests = sequelize.define("guests", {
      guest_id: {
         allowNull: false,
         autoIncrement: true,
         primaryKey: true,
         type: Sequelize.INTEGER
      },
      name: {
         type: Sequelize.STRING,
         allowNull: false,
      },
      email: {
         type: Sequelize.STRING,
         allowNull: false,
      }
   }, {
      freezeTableName: true,
      timestamps: false,
      underscored: true
   })
   return guests
}
