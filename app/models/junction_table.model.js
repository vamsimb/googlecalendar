module.exports = (sequelize, Sequelize) => {
   const junction_table = sequelize.define("junction_table", {
      id: {
         allowNull: false,
         autoIncrement: true,
         primaryKey: true,
         type: Sequelize.INTEGER
      },
   },
      {
         freezeTableName: true,
         underscored: true,
         timestamps: false,
      }
   );
   return junction_table;
};

// collection in post man