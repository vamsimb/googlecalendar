module.exports = (sequelize, Sequelize) => {
   const Events = sequelize.define("events", {
      id: {
         allowNull: false,
         autoIncrement: true,
         primaryKey: true,
         type: Sequelize.INTEGER
      },
      title: {
         type: Sequelize.STRING,
         allowNull: false,
      },
      hosted_by: {
         type: Sequelize.STRING,
         allowNull: false,
      },
      place: {
         type: Sequelize.STRING,
         allowNull: false,
      },
      date: {
         type: Sequelize.DATE,
         allowNull: false,
         defaultValue: "2022-01-01"
      },
      start_time: {
         type: Sequelize.TIME,
         allowNull: false,
      },
      end_time: {
         type: Sequelize.TIME,
         allowNull: false,
      },
   }, {
      freezeTableName: true,
      timestamps: false,
      underscored: true
   });

   return Events;
}