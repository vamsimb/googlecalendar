module.exports = app => {
   const events = require("../controllers/events.controller.js");
   const guests = require("../controllers/guests.controller.js");
   let router = require("express").Router();

   // All Get routes

   router.get("/", events.findAllEvents);

   router.get("/guests", guests.findAllGuestNames);

   router.get("/guests/:id", guests.findGuestAndHisEvents);

   router.get("/:id", events.findOneEvent);

   // Post events

   router.post("/", events.createEvent);

   app.use("/api/events", router);
}