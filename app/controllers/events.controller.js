const db = require("../models")
const Events = db.events
const Guests = db.guests
const Op = db.Sequelize.Op;

// events section
exports.findAllEvents = async (req, res) => {

  try {
    const events = await Events.findAll({
      include: [
        {
          model: Guests,
          through: {
            attributes: []
          },
          attributes: ['name', 'email']
        }
      ]
    })
    if (events) {
      return res.send(events)
    } else {
      return res.send([])
    }
  }

  catch (error) {
    return res.status(403).send({
      message: error.message || "Some error occurred while Showing all events data",
    })
  }
}

exports.findOneEvent = async (req, res) => {

  try {
    const id = req.params.id;
    const event = await Events.findByPk(id, {
      include: [
        {
          model: Guests,
          through: {
            attributes: []
          },
          attributes: ['name', 'email']
        }
      ]
    })
    if (event) {
      return res.send(event)
    } else {
      return res.send([])
    }
  }

  catch (error) {
    return res.status(403).send({
      message: "Error retrieving event with id=" + id
    })
  }
}


exports.createEvent = async (req, res) => {

  try {
    const {
      hosted_by,
      place,
      end_time,
      start_time,
      title,
      guests
    } = req.body

    // Check if the time schedules are available

    function message(message) {
      return res.status(403).json({
        message: message
      })
    }

    if (start_time >= end_time) {
      return message("Start time must be before end time")
    }

    if (!hosted_by || hosted_by.trim().length === 0) {
      return message('The host cannot be an empty string.')
    }

    if (!guests || guests.length === 0) {
      return message("At least one guest is required.")
    }

    // Check if the host is available
    const hostEvents = await Events.findAll({
      where: {
        hosted_by: hosted_by,
        start_time: {
          [Op.lt]: end_time
        },
        end_time: {
          [Op.gt]: start_time
        }
      }
    })

    if (hostEvents.length > 0) {
      return message('The host is not available during the selected time period.')
    }

    //Section: Check if all the host and guests are available

    function personAvailability(person) {
      return Events.findAll({
        include: [
          {
            model: Guests,
            where: {
              name: person
            }
          }
        ],
        where: {
          start_time: {
            [Op.lt]: end_time
          },
          end_time: {
            [Op.gt]: start_time
          }
        }
      })
    }

    function guestRecords(guest) {
      return Guests.findOne({
        where: {
          name: guest
        }
      })
    }

    const hostEvent = await personAvailability(hosted_by)

    if (hostEvent.length > 0) {
      return message(`host ${hosted_by} is not available during the selected time period.`)
    }

    // const addHost = await guestRecords(hosted_by)
    // if (!addHost) {
    //   await Guests.create({
    //     name: hosted_by,
    //     email: `${hosted_by}@gmail.com`
    //   })
    // }

    const guest_ids = []

    for (const guest of guests) {
      let guestRecord = await guestRecords(guest)

      if (!guestRecord) {
        guestRecord = await Guests.create({
          name: guest,
          email: `${guest}@gmail.com`
        })
      }

      const guestEvents = await personAvailability(guest)

      if (guestEvents.length > 0) {
        return message(`guest ${guest} is not available during the selected time period.`)
      }

      currentGuest = await Guests.findOne({
        where: {
          name: guest
        }
      })
      guest_ids.push(currentGuest.guest_id)
    }


    const event = await Events.create({
      hosted_by,
      place,
      end_time,
      start_time,
      title
    })

    event.setGuests(guest_ids)
    return res.status(201).json({
      message: 'Event created successfully.'
    })

  }

  catch (error) {
    return res.status(500).json({
      message: 'Something went wrong. Please try again later.'
    })
  }
}
