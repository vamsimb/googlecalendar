const db = require("../models")

const Events = db.events
const Guests = db.guests
// const Op = db.Sequelize.Op;
// const junction_table = db.junction_table

// For Guest List data (names and their IDs)

exports.findAllGuestNames = async (req, res) => {
   try {
      const data = await Guests.findAll({
         include: [
            {
               model: Events,
               through: {
                  attributes: []
               },
               attributes: ['title', 'hosted_by', "place", "start_time", "end_time"]
            }
         ]
      })

      if (data) {
         return res.send(data)
      } else {
         return res.send([])
      }
   }
   catch (err) {
      return res.status(403).send({
         message: "Some error occurred while retrieving guests name list."
      })
   }
}


exports.findGuestAndHisEvents = async (req, res) => {
   try {
      const id = req.params.id
      const data = await Guests.findByPk(id, {
         include: [
            {
               model: Events,
               through: {
                  attributes: []
               },
               attributes: ['title', 'hosted_by', "place", "start_time", "end_time"]
            }
         ]
      })

      if (data) {
         return res.send(data)
      } else {
         return res.send([])
      }
   }
   catch (error) {
      res.status(403).send({
         message: " Some error occurred while retrieving guests events with id=" + id
      })
   }
}