const express = require("express")
const cors = require("cors")
const dotenv = require("dotenv")
const app = express()
dotenv.config()
const PORT = process.env.PORT || 5000

app.use(cors())

app.use(function (req, res, next) {
   res.setHeader("Access-Control-Allow-Origin", "*")
   res.setHeader("Access-Control-Allow-Credentials", "true")
   res.setHeader(
      "Access-Control-Allow-Methods",
      "GET,HEAD,OPTIONS,POST,PUT,DELETE"
   )
   res.setHeader("Access-Control-Allow-Headers", "*")

   if (req.method === "OPTIONS") {
      return res.status(200).end()
   }
   next()
})

app.use(express.json())

app.use(express.urlencoded({
   extended: true
}));

const db = require("./app/models")
db.sequelize.sync()
   .then(() => {
      console.log("Synced db.")
   })
   .catch((err) => {
      console.log("Failed to sync db: " + err.message)
   });


app.get("/", (req, res) => {
   res.json({
      message: "Welcome to google calendar v1.2"
   })
});

require("./app/routes/events.routes")(app);


app.listen(PORT, () => {
   console.log(`Server is running on port ${PORT}.`)
});
